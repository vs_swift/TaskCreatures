//
//  Runner.swift
//  TaskCreatures
//
//  Created by Private on 12/22/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Runner: Human, Runners{
    
    var kmQty: Int
    
    var distance: Int {
        return kmQty
    }
        
    func run() {
        return specialMove()
    }
        
    func stroll() {
        print ("When i am tired i can go slowly")
    }
    
    init(name: String, height: Double, weight: Int, gender: String, kmQty: Int) {
        self.kmQty = kmQty
        
        super.init(name: name, height: height, weight:weight, gender: gender)
    }
    func specialMove(){
        print("I can run now!")
    }
    override func move(){
        super.move()
        run()
        stroll()
    }
}
