//
//  Snowboarder.swift
//  TaskCreatures
//
//  Created by Private on 12/22/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Snowboarder: Human, Jumpers {
    
    var maxJump: Int = 3
    
    var JumpHeight: Int {
        return maxJump
    }
    
    func jump() {
        print ("I can jump")
        
    }
    func backflip() {
        print ("I can do backflips on my snowboard")
    }
    
    let tool: String
    let color: String
    let destination: String
    
    init(name: String, height: Double, weight: Int, gender: String, tool: String,color: String,  destination: String, maxJump: Int) {
        self.tool = tool
        self.color = color
        self.destination = destination
        self.maxJump = maxJump
        
        super.init(name: name, height: height, weight:weight, gender: gender)
    }
    func specialMove(){
        print("I can do snowboarding now!")
    }
    override func move(){
        super.move()
        specialMove()
        jump()
        backflip()
    }
    override func display(){
        print("\nName: \(name)\nHeight: \(height)\nWeight: \(weight)\nGender: \(gender)\nTool: \(tool)\nColor: \(color)\nDestination: \(destination)")
    }
}
