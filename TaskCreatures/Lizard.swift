//
//  Lizard.swift
//  TaskCreatures
//
//  Created by Private on 12/22/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Lizard: Animal, Swimmers {
    
    var maxDepth: Int = 10
    var depth: Int {
        return maxDepth
    }
    
    func swimm() {
        print ("i enjoy Swimmming")
    }
    func dive() {
        print ("I can hold my breath and dive really deep")
    }
    
    init(title: String, length: Double,numberOfLimbs: Int, maxDepth: Int) {
        self.maxDepth = maxDepth
        
        super.init(title: title, length: length,numberOfLimbs: numberOfLimbs)
    }
    
    func specialMove(){
        print("+ I can crawl")
    }
    override func move(){
        super.move()
        specialMove()
        swimm()
        dive()
    }
}
