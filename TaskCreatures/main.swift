//
//  main.swift
//  TaskCreatures
//
//  Created by Private on 12/21/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

let Homo = Human(name:"Homo sapien",height: 1.5, weight: 100, gender: "Male")
let Bolt = Runner(name:"Usain Bolt",height: 1.9, weight: 70, gender:"Male",kmQty: 10)
let Beard = Swimmer(name:"Amanda Beard",height: 1.8, weight: 65, gender:"Female", maxDepth: 3)
let White = Snowboarder(name:"Shaun White", height: 1.8, weight: 70, gender: "Male", tool: "Snowboard", color: "Red", destination: "Austria", maxJump: 3)

let Joey = Kangaroo(title: "Joey", length: 0.7 ,numberOfLimbs: 2, maxJump: 5)
let Caiman = Lizard(title: "Caiman", length: 5, numberOfLimbs: 4, maxDepth: 5)
let Charlie = Jaguar(title: "Charlie", length: 2, numberOfLimbs: 4, maxJump: 5, maxDepth: 2, kmQty: 100)


var Everybody: [Generic] = [Homo,Bolt,Beard,White,Joey,Caiman,Charlie]

for value in Everybody {
    if let h = value as? Runners {
        print("\nRunner")
        print(h.display())
        print(h.run())
        print(h.stroll())
    } else if let b = value as? Swimmers {
        print("\nSwimmer")
        print(b.display())
        print(b.swimm())
        print(b.dive())
    } else if let a = value as? Jumpers {
    print("\nJumper")
    print(a.display())
    print(a.jump())
    print(a.backflip())
    } else {
        print("Негр")
        value.display()
    }
}


