//
//  Kenguru.swift
//  TaskCreatures
//
//  Created by Private on 12/22/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Kangaroo: Animal,Jumpers {
    
    var maxJump: Int
    
    var JumpHeight : Int {
        return maxJump
    }

    func jump() {
        print ("I am a great jumper")
    }
    func backflip() {
        print ("I can do BackFlips")
    }
    
    init(title: String, length: Double,numberOfLimbs: Int,maxJump: Int) {
        self.maxJump = maxJump
        
        super.init(title: title, length: length,numberOfLimbs: numberOfLimbs)
    }
    
    func specialMove(){
        print("+ I live in Australia and you don't!!! ")
    }
    override func move(){
        super.move()
        specialMove()
        jump()
        backflip()
    }
}
