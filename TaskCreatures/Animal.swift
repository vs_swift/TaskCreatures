//
//  Animal.swift
//  TaskCreatures
//
//  Created by Private on 12/22/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Animal : Generic {
    var title: String
    var length: Double
    var numberOfLimbs: Int
    
    init(title: String, length: Double,numberOfLimbs: Int) {
        self.title = title
        self.length = length
        self.numberOfLimbs = numberOfLimbs
    }
    
    func move(){
        print("I can do nothing")
    }
    
    func display(){
        print("\nTitle: \(title)\nLength: \(length)\nNumber Of Limbs: \(numberOfLimbs)")
    }
}
