//
//  Jaguar.swift
//  TaskCreatures
//
//  Created by Private on 12/24/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Jaguar: Animal,Runners,Jumpers,Swimmers {
    
    var maxJump: Int = 3
    var JumpHeight: Int {
        return maxJump
    }
    
    var kmQty: Int = 100
    var distance: Int {
        return kmQty
    }
    
    var maxDepth: Int = 3
    var depth: Int {
        return maxDepth
    }
    
    func jump() {
        print ("I can jump")
    }
    func backflip() {
        print ("I can do backflips but i don't want to")
    }
    func run() {
        return specialMove()
    }
    func stroll() {
        print ("When i am tired i can go slowly")
    }
    func swimm(){
        print ("i enjoy Swimmming")
    }
    func dive(){
        print ("I can hold my breath and dive really deep")
    }
        
    init(title: String, length: Double,numberOfLimbs: Int, maxJump: Int, maxDepth: Int, kmQty:Int) {
        self.maxJump = maxJump
        self.maxDepth = maxDepth
        self.kmQty = kmQty
        
        super.init(title: title, length: length,numberOfLimbs: numberOfLimbs)
    }

    func specialMove(){
        print("I can run with a spead 90 miles per hour and you can't!!!")
    }
    override func move(){
        super.move()
        specialMove()
    }
}
