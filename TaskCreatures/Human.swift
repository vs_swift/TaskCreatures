//
//  Human.swift
//  TaskCreatures
//
//  Created by Private on 12/22/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Human: Generic {
    var name: String
    var height: Double
    var weight: Int
    var gender: String
    
    init(name: String, height: Double, weight: Int, gender: String) {
        self.name = name
        self.height = height
        self.weight = weight
        self.gender = gender
    }
    
    func move(){
        print ("I can do nothing!")
    }
    
    func display(){
        print("\nName: \(name)\nHeight: \(height)\nWeight: \(weight)\nGender: \(gender)")
    }
}
