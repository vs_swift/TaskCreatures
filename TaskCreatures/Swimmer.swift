//
//  Swimmer.swift
//  TaskCreatures
//
//  Created by Private on 12/22/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

class Swimmer: Human, Swimmers{
    
    var maxDepth: Int = 3
    
    var depth: Int {
        return maxDepth
    }
    
    func swimm() {
        specialMove()
    }
    func dive() {
        print ("I can hold my breath and dive really deep")
    }
    
    init(name: String, height: Double, weight: Int, gender: String, maxDepth: Int) {
        self.maxDepth = maxDepth
        
        super.init(name: name, height: height, weight:weight, gender: gender)
    }
    
    func specialMove(){
        print("I can swimm now!")
    }
    
    override func move(){
        super.move()
        swimm()
        dive()
    }
}
