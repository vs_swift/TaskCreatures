//
//  Protocols.swift
//  TaskCreatures
//
//  Created by Private on 12/22/17.
//  Copyright © 2017 Private. All rights reserved.
//

import Foundation

protocol Generic {
    func display()
}

protocol Jumpers : Generic {
    var JumpHeight: Int {get}
    func jump()
    func backflip()
}

protocol Swimmers : Generic {
    var depth: Int {get}
    func swimm()
    func dive()
}

protocol Runners : Generic {
    var distance: Int {get}
    func run()
    func stroll()
}

